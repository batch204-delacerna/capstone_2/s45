const Users = require("../models/Users");
const Products = require("../models/Products");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration
module.exports.userRegistration = (requestBody) => {

	// Validation for existing users
	return Users.findOne({email: requestBody.email}).then(result => {

		if (result !== null) {
			
			//return false //For REACT.js
			return "User email already exists!";

		} else {

			let newUser = Users({
				email: requestBody.email,
				password: bcrypt.hashSync(requestBody.password, 10),
				isAdmin: requestBody.isAdmin
			});

			return newUser.save().then((user, error) => {

				if (error) {
					//return false //For REACT.js
					console.log(error);
					return "User registration failed!";
				} else {
					// return true //For REACT.js
					return `User registration successful! User ${newUser.email} created!`;
				}


			});
		}

	});

}

// User Authentication
module.exports.userLogin = (requestBody) => {

	return Users.findOne({email: requestBody.email}).then(result => {

		if (result == null) {
			//return false //For REACT.js
			return "User does not exist, please signup first.";
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return "Incorrect password."
			}

		}

	});

}

// Create Order
module.exports.createUserOrder = async (data) => {

	if (data.isAdmin === true) {
		return false
	} else {

		let productPrice = await Products.findById(data.productId).then(products => products.price);

		// Save to Users
		const usersCreateOrder = await Users.findById(data.userId).then(users => {

			let totalAmountDue = (productPrice) * (data.quantity);

			users.order.push({
				products: {productName: data.productName, 
						   quantity: data.quantity},
				totalAmount: totalAmountDue
				});

			return users.save().then((users, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			});


		});
		// End of Save to Users


		// Save to Products
		const productsCreateOrder = await Products.findById(data.productId).then(products => {

			products.orders.push({orderId: data.userId});

			return products.save().then((products, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			});

		}); 
		// End of Save to Products

		if(usersCreateOrder && productsCreateOrder) {
			return true
		} else {
			return false
		}


	} //end of else

}

module.exports.getUserDetails = (userId) => {

	return Users.findById(userId).then(result => {
			result.password = "";

			return result;
	});


}

module.exports.getAllOrders = (userData) => {

	return Users.findById(userData.id).then(result => {

		if (userData.isAdmin !== true) {
			return false
		} else {

			// return users.order.find({})
			return Users.find({isAdmin: false}, {order: 1}).then(result => {

				return result;

			})


		} //end of else

	});

}


module.exports.getMyOrders = (userData) => {

	return Users.findById(userData.id).then(result => {

		if (userData.isAdmin === true) {
			return false
		} else {

			// return users.order.find({})
			return Users.find({_id: userData.id}, {order: 1}).then(result => {

				return result;

			})


		} //end of else

	});
}

module.exports.setUserAsAdmin = (requestParams, userData) => {

	return Users.findById(userData.id).then(result => {

		if (userData.isAdmin !== true || userData.email !== "admin@mail.com") {
			return false
		} else {
			
			let usersInfoUpdate = {
				isAdmin: true
			}
			console.log(requestParams);
			return Users.findByIdAndUpdate(requestParams.userId, usersInfoUpdate).then((user, error) => {

				if (error) {
				    return false
				} else {
					return true
				}
			});

		}

	});

}

module.exports.removeAsAdmin = (requestParams, userData) => {

	return Users.findById(userData.id).then(result => {

		if (userData.isAdmin !== true || userData.email !== "admin@mail.com") {
			return false
		} else {
			
			let usersInfoUpdate = {
				isAdmin: false
			}
			console.log(requestParams);
			return Users.findByIdAndUpdate(requestParams.userId, usersInfoUpdate).then((user, error) => {

				if (error) {
				    return false
				} else {
					return true
				}
			});

		}

	});

}