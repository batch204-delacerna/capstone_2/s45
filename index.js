const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Require routes here
const usersRoute = require("./routes/usersRoute");
const productsRoute = require("./routes/productsRoute");


// Before deployment in HEROKU
// const port = process.env.PORT || 4000;
const port = 4000;

const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://admin:admin123@batch204-delacernamikki.vn2dzfk.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true});

let dbConnection = mongoose.connection;
dbConnection.on("error", () => console.error.bind(console, "Error in connecting to the Database!"));
dbConnection.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// Middlewares
app.use(cors());
app.use(express.json());

// Define endpoints based on the routes here
app.use("/users", usersRoute);
app.use("/products", productsRoute);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});