const express = require("express");
const router = express.Router();
const productsController = require("../controllers/productsController");
const authentication = require("../auth");

// S43 - Create a product
// Route for creating a product
router.post("/", authentication.tokenVerification, (request, response) => {

	const usersData = authentication.tokenDecryption(request.headers.authorization);

	productsController.productRegistration(request.body, {userId: usersData.id, isAdmin: usersData.isAdmin}).then(controllerResult => response.send(controllerResult));
});

 
// S43 - Retrieve all products
// Route for retrieving all active products
router.get("/", (request, response) => {

	productsController.activeProductRetrieval().then(controllerResult => response.send(controllerResult));

});


// S44 - Retrieve a single product
// Route for retrieving a single product
router.get("/:productId", (request, response) => {

	productsController.getProduct(request.params).then(controllerResult => response.send(controllerResult));

});


// S44 - Update Product information (Admin only)
// Route for updating product information
router.put("/:productId", authentication.tokenVerification, (request, response) => {

	const usersData = authentication.tokenDecryption(request.headers.authorization);

	productsController.updateProductInfo(request.params, request.body, usersData).then(controllerResult => response.send(controllerResult));

});

// S44 - Archive Product (Admin only)
// Route for archiving a product
router.put("/:productId/archive", authentication.tokenVerification, (request, response) => {

	const usersData = authentication.tokenDecryption(request.headers.authorization);

	productsController.archiveProduct(request.params, usersData).then(controllerResult => response.send(controllerResult));

});


// Route for activating a product
router.put("/:productId/enable", authentication.tokenVerification, (request, response) => {

	const usersData = authentication.tokenDecryption(request.headers.authorization);

	productsController.enableProduct(request.params, usersData).then(controllerResult => response.send(controllerResult));

});



module.exports = router;